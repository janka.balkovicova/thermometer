# Thermometer

The thermometer application is designed to be used with NodeMCU module and embedded thermal sensor DS18B20. Programming of NodeMCU microcontroller is not part of this repository. 

Embedded system has WiFi access point, which is listening to application connections. NodeMCU is programmed to be fully autonomous system, the only part the user can change is the interval of measurements. Default measurement time is set to 15s so temperature change could be easily demonstrated in demo.

Application shows measured temperature in °C and °F. Refresh button sends request for last measured temperature. Measurements in history are ordered by seconds since NodeMCU started, and every restart of module makes a new session.

## Screencasts

![Screencast 1](img/screencast1.gif)

![Screencast 2](img/screencast2.gif)

![Screencast 3](img/screencast3.gif)