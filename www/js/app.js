/*  Jana Balkovicová
 *  xbalko01@stud.fit.vutbr.cz
 *  ESP8266: snímání teploty (IoT, WiFi AP pro mobilní telefon), IMP 2018
 *  original: 90%
 *  last edited: 27.12.2018
 */

function celsiusToFarhenheit(tempC) {
    return tempC * 1.8 + 32.0;
}

function createCard(element, temp, ticker) {
    let $cardPanel = $([
        '<div class="card-panel" style="margin: 10px">',
        '<h4 style="margin: 0 !important;">' + temp +'°C</h4>',
        '<span>' + celsiusToFarhenheit(temp).toFixed(2) + '°F </span>',
        '<span class="right" style="font-style: italic; color: #606060; font-size: 12px">' + ticker + ' seconds from start</span>',
        '</div>'
    ].join("\n"));
    element.append($cardPanel);
}

function drawChart(chartId) {
    return new Chart(document.getElementById(chartId), {
        type: 'line',
        "data": {
            "labels": "",
            "datasets": [{
                "label": "Temperature flow",
                "data": "",
                "fill": false,
                "borderColor": "rgb(75, 192, 192)",
                "lineTension": 0.1
            }]
        },
        "options": {
            spanGaps: true
        }
    });
}

// Taken from Chart.js example page and edited "push" to "unshift"
function addData(chart, label, data) {
    chart.data.labels.unshift(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.unshift(data);
    });
    chart.update();
}