/*  Jana Balkovicová
 *  xbalko01@stud.fit.vutbr.cz
 *  ESP8266: snímání teploty (IoT, WiFi AP pro mobilní telefon), IMP 2018
 *  original: 70%
 *  last edited: 27.12.2018
 */

#include <DallasTemperature.h>
#include <EEPROM.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <OneWire.h>
#include <Ticker.h>
#include <WiFiClient.h>

#define LED_PIN 2
#define THERMOMETER_PIN 5

OneWire thermometerWire(THERMOMETER_PIN);
DallasTemperature thermometerSensors(&thermometerWire);

//https://github.com/esp8266/Arduino/blob/master/libraries/EEPROM/examples/eeprom_write/eeprom_write.ino
int addr = 0; // the current address in the EEPROM (i.e. which byte we're going to write to next)
float tempC = 0;
unsigned int bootTime = 0;
unsigned int timePoint = 0;
int historyMeasures = 0; // Count
int measures = 0;
int tickerTime = 15; // Measuring interval

Ticker thermometer;

// WiFi parameters
const char* ssid = "ESP8266_softAP";
const char* password = "thermometer";

ESP8266WebServer server(80);

String json;
String historyJson;
String currentJson;

// Taken from ESP examples
void EEPROMclear() {
  for (int i = 0 ; i < EEPROM.length() ; i++) {
    EEPROM.write(i, 0);
    EEPROM.commit();
  }
}

// Taken from ESP examples
void blinkLED() {
  digitalWrite(LED_PIN, LOW);   // Turn the LED on (Note that LOW is the voltage level but actually the LED is on; this is because
  //it is active low on the ESP-01)
  delay(1000);            // Wait for a second
  digitalWrite(LED_PIN, HIGH);  // Turn the LED off by making the voltage HIGH
}

float measureTemp() {
  thermometerSensors.requestTemperatures(); // Get data from all connected sensors
  tempC = thermometerSensors.getTempCByIndex(0); // We have just one sensor => index 0
  timePoint = millis() / 1000 - bootTime;
  Serial.print(timePoint);
  Serial.print("s, temperature: ");
  Serial.println(tempC);
  EEPROM.put(addr, tempC);
  EEPROM.commit();
  addr += sizeof(tempC);
  EEPROM.put(addr, timePoint);
  EEPROM.commit();
  addr += sizeof(timePoint);
  digitalWrite(LED_PIN, !(digitalRead(LED_PIN))); // Switch LED so it's visible that temperature was measured
  if (measures != 0) {
    currentJson += ",";
  }
  currentJson += "{\"temperature\": ";
  currentJson += tempC;
  currentJson += ", \"ticker\": ";
  currentJson += timePoint;
  currentJson += "}";
  measures += 1;
}

void loadHistory() {
  int i = 0;
  float historyTemp = 0.0f;
  unsigned int historyTime = 0;
  for (int i = 0 ; i < EEPROM.length() ; ) {
    EEPROM.get(i, historyTemp);
    i += sizeof(historyTemp);
    EEPROM.get(i, historyTime);
    i += sizeof(historyTime);
    if (historyTemp == 0.0 and historyTime == 0) { // If there are no data, we have loaded the whole history
      return;
    }
    else {
      if (historyMeasures != 0) {
        historyJson += ",";
      }
      historyJson += "{\"temperature\": ";
      historyJson += historyTemp;
      historyJson += ",";
      historyJson += "\"ticker\": ";
      historyJson += historyTime;
      historyJson += "}";
      historyMeasures += 1;
    }
  }
}

void setup() {
  Serial.begin(115200);
  // https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/soft-access-point-examples.html
  Serial.print("Setting soft-AP ");
  Serial.print(ssid);
  Serial.print(" ... ");
  boolean connected = WiFi.softAP(ssid, password);

  if (connected == true) {
    Serial.print("Ready on ");
    Serial.println(WiFi.softAPIP()); // Print the IP address

    thermometerSensors.begin(); // Start measuring session
    Serial.println("Sensors are active.");

    EEPROM.begin(4096);
    pinMode(LED_PIN, OUTPUT);

    server.on("/", [](){
      json = "{\"temperature\": ";
      json += tempC;
      json += ", \"ticker\": ";
      json += timePoint;
      json += "}";
      server.send(200, "application/json", json);
      Serial.println("Got a connection.");
      Serial.println(json);
    });

    server.on("/ticker.json", [](){
      json = "{\"interval\": ";
      json += tickerTime;
      json += "}";
      server.send(200, "application/json", json);
      Serial.println("'ticker' JSON sent.");
      Serial.println(json);
    });

    server.on("/setticker", [](){
      String data = server.arg(0);
      data = data.substring(9);
      tickerTime = data.toInt();
      thermometer.attach(tickerTime, measureTemp);
    });

    server.on("/history.json", [](){
      json = "{\"history\": [";
      json += historyJson;
      json += "]}";
      server.send(200, "application/json", json);
      Serial.println("'history' JSON sent.");
      Serial.println(json);
    });

    server.on("/current.json", [](){
      json = "{\"current\": [";
      json += currentJson;
      json += "]}";
      server.send(200, "application/json", json);
      Serial.println("'current' JSON sent.");
      Serial.println(json);
    });

    loadHistory(); // Get measures from EEPROM to historyJson
    addr = historyMeasures * (sizeof(tempC) + sizeof(timePoint)); // Get the first free EEPROM address

    server.begin(); // Start the server
    Serial.println("Server started");

    bootTime = millis() / 1000; // Set boot time for correct measuring time

    thermometer.attach(tickerTime, measureTemp); // Set measuring interval to 15 seconds
    Serial.println("Thermometer ticker attached.");
  }
}

void loop() {
  server.handleClient();
}